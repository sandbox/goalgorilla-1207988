/**
 * @file
 * Replaces FullCalendar drupal plugin.
 */

(function ($) {

Drupal.behaviors.fullCalendarExtra = function(context) {

  if (window.location.hash){
    var splittedLoad = window.location.hash.toString();
    var splittedLoad = splittedLoad.substr(1).split('/');
  }

  if (Drupal.settings.fullcalendar_extra.msg) {
    if ($('.fullcalendar-status').text() === '') {
      $('.fullcalendar-status').html(Drupal.settings.fullcalendar_extra.msg).slideDown();
    } else {
      $('.fullcalendar-status').html(Drupal.settings.fullcalendar_extra.msg);
      $('.fullcalendar-status').html(Drupal.settings.fullcalendar_extra.msg).effect('highlight', {}, 500);
    }
  }
  // Process each view and its settings.
  $.each(Drupal.settings.fullcalendar, function(index, settings) {
    // Hide the failover display.
    $(index).find('.fullcalendar-content').hide();

    // Add events from Drupal.
    var eventSourcesArray = [
      function(start, end, callback) {
        var events = [];
        $(index).find('.fullcalendar-event-details').each(function() {
          events.push({
            field: $(this).attr('field'),
            index: $(this).attr('index'),
            nid: $(this).attr('nid'),
            title: $(this).attr('title'),
            start: $(this).attr('start'),
            end: $(this).attr('end'),
            url: $(this).attr('href'),
            allDay: ($(this).attr('allDay') === '1'),
            className: $(this).attr('cn'),
            editable: $(this).attr('editable'),
            dom_id: index
          });
        });
        callback(events);
      }
    ];

    // Add events from Google Calendar feeds.
    $.each(settings.gcal, function(i, gcalEntry) {
      eventSourcesArray.push($.fullCalendar.gcalFeed(gcalEntry[0], gcalEntry[1]));
    });

    if (settings.selectable == 1) {
      selectableVar = settings.createPermission; 
    } else {
      selectableVar = false; 
    }
    // Use :not to protect against extra AJAX calls from Colorbox.
    var fullcalendar = $(index).find('.fullcalendar:not(.fc-processed)').addClass('fc-processed').fullCalendar({
      defaultView: settings.defaultView,
      theme: settings.theme,
      header: {
        left: settings.left,
        center: settings.center,
        right: settings.right
      },
      isRTL: settings.isRTL === '1',
      eventClick: function(calEvent, jsEvent, view) {
        if (settings.colorbox) {
          // Open in colorbox if exists, else open in new window.
          if ($.colorbox) {
            var url = calEvent.url;
            if(settings.colorboxClass !== '') {
              url += ' ' + settings.colorboxClass;
            }
            $.colorbox({
              href: url,
              width: settings.colorboxWidth,
              height: settings.colorboxHeight
            });
          }
        }
        else {
          if (settings.sameWindow) {
            window.open(calEvent.url, '_self');
          } else if (settings.modalframe) {
            var url = calEvent.url;
            var modalOptions = {
              url: url  + '/fullcalendar_extra',
              autoFit: true,
              //onSubmit: onSubmitCallback,
            };        
        
            Drupal.modalFrame.open(modalOptions);
          }
          else {
            window.open(calEvent.url);
          }
        }
        return false;
      },
      year: (settings.year) ? settings.year : undefined,
      month: (settings.month) ? settings.month : undefined,
      day: (settings.day) ? settings.day : undefined,
      timeFormat: {
        agenda: (settings.clock) ? 'HH:mm{ - HH:mm}' : settings.agenda,
        '': (settings.clock) ? 'HH:mm' : settings.agenda
      },
      
      // Adding our own variables [Fullcalendar_extra]
      selectable: Number(selectableVar),
      selectHelper: Drupal.settings.fullcalendar.createPermission,
      minTime: settings.min,
      allDaySlot: Number(settings.all_day_slot),  
      slotMinutes: Number(settings.slot_minutes),
      defaultEventMinutes: settings.default_event_minutes,
      maxTime: settings.max,
      weekends: Number(settings.weekends),
      // End [Fullcalendar_extra]
      
      axisFormat: (settings.clock) ? 'HH:mm' : 'h(:mm)tt',
      weekMode: settings.weekMode,
      firstDay: settings.firstDay,
      monthNames: settings.monthNames,
      monthNamesShort: settings.monthNamesShort,
      dayNames: settings.dayNames,
      dayNamesShort: settings.dayNamesShort,
      allDayText: settings.allDayText,
      buttonText: {
        today: settings.todayString,
        day: settings.dayString,
        week: settings.weekString,
        month: settings.monthString
      },
      eventSources: eventSourcesArray,
      eventRender: function(event, element) {

        if (event.editable == 1) {
          element.find('.fc-event-time').after(
          '<div class="fc-event-action">' +
            '<img src="'+Drupal.settings.basePath+Drupal.settings.fullcalendar_extra.modulePath+'/images/menu.png" />' +
            '<div class="tools">' +
              '<a class="delete" href="' + event.url + '/delete" >' +
                '<img src="'+Drupal.settings.basePath+Drupal.settings.fullcalendar_extra.modulePath+'/images/delete.png" />' +
              '</a>' +
              '<a class="edit" href="' + event.url + '/edit" >' +
                '<img src="'+Drupal.settings.basePath+Drupal.settings.fullcalendar_extra.modulePath+'/images/edit.png" />' +
              '</a>' +
            '</div>' +
          '</div>');
          
          element.find('.fc-event-action .tools a').click(function(e) {
            var modalOptions = {
              url: $(this).attr('href')  + '/fullcalendar_extra_update',
              autoFit: true,
              onSubmit: onSubmitCallback,
            };        
        
            function onSubmitCallback(args, statusMessages) {
              if (args) {
                window.location.reload();
              }        
            }        
            Drupal.modalFrame.open(modalOptions);
            return false;
          });
        }      
      },
      eventMouseover: function(calEvent, jsEvent, view) {
      	$(this).css('z-index', '10');	    	
      },   
      eventMouseout: function(calEvent, jsEvent, view) {
        $(this).css('z-index', '8');
      },
      eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc) {
        $.post(Drupal.settings.basePath + 'fullcalendar/ajax/update/drop/'+ event.nid,
          'field=' + event.field + '&index=' + event.index + '&day_delta=' + dayDelta + '&minute_delta=' + minuteDelta + '&all_day=' + allDay + '&dom_id=' + event.dom_id,
          fullcalendarUpdate);
        return false;
      },
      eventResize: function(event, dayDelta, minuteDelta, revertFunc) {
        $.post(Drupal.settings.basePath + 'fullcalendar/ajax/update/resize/'+ event.nid,
          'field=' + event.field + '&index=' + event.index + '&day_delta=' + dayDelta + '&minute_delta=' + minuteDelta + '&dom_id=' + event.dom_id,
          fullcalendarUpdate);
        return false;
      },
      select: function( startDate, endDate, allDay, jsEvent, view ) {

        function onSubmitCallback(args, statusMessages) {
         if (args) {
            fullcalendar.fullCalendar('renderEvent', 
            {
              start: args.start,
  				    end: args.end,
  				    allDay: false,
  				    editable: args.editable,
  				    className: args.className,
  				    index: args.index,
  				    nid: args.nid,
  				    url: Drupal.settings.basePath + 'node/' + args.nid,
	   			    title: args.title,
	   			    field: args.field,
              dom_id: index,
            }, true);
            if ($('.fullcalendar-status').text() === '') {
              $('.fullcalendar-status').html(args.msg).slideDown();
            } else {
              $('.fullcalendar-status').html(args.msg);
              $('.fullcalendar-status').html(args.msg).effect('highlight', {}, 500);
            }
          }        
        }
        
        fullcalendar.fullCalendar('unselect');

        var modalOptions = {
          url: Drupal.settings.basePath + 'node/add/' + settings.fce_content_type + '/fullcalendar_extra?startDate=' + Date.parse(startDate) + '&endDate=' +  Date.parse(endDate) + '&field=' + settings.fce_field,
          autoFit: true,
          onSubmit: onSubmitCallback,
        };        
        
        Drupal.modalFrame.open(modalOptions);
        return false;
      },
      viewDisplay: function(view) {
        if (!splittedLoad) {
          window.location.hash = view.start.getDate() + '/' + (view.start.getMonth() + 1) + '/' + view.start.getFullYear();
        }      
      },
    });
  });

  // Bind the event.
  $(window).hashchange( function(){
    if (!splittedLoad) {
      var view = $('.fullcalendar').fullCalendar('getView');
      if (window.location.hash != '#' + view.start.getDate() + '/' + (view.start.getMonth() + 1) + '/' + view.start.getFullYear()) {
        var splitted = window.location.hash.toString();
        var splitted = splitted.substr(1).split('/');
        $('.fullcalendar').fullCalendar( 'gotoDate', splitted[2], (splitted[1] - 1), splitted[0]);
      }
    }
  });

  var fullcalendarUpdate = function(response) {
    var result = Drupal.parseJson(response);
    fcStatus = $(result.dom_id).find('.fullcalendar-status');
    if (fcStatus.text() === '') {
      fcStatus.html(result.msg).slideDown();
    }
    else {
      fcStatus.html(result.msg);
      fcStatus.effect('highlight', {}, 500);
    }
    return false;
  };

  $('.fullcalendar-status-close').live('click', function() {
    $(this).parent().slideUp();
    return false;
  });
  if (splittedLoad) {
    $('.fullcalendar').fullCalendar( 'gotoDate', splittedLoad[2], (splittedLoad[1] - 1), splittedLoad[0]);
    splittedLoad = null;
  }
  // Trigger the event (useful on page load).
  //$(window).hashchange();
  // Trigger a window resize so that calendar will redraw itself as it loads funny in some browsers occasionally
  $(window).resize();
};

})(jQuery);
